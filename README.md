# dotfiles #

CentOS環境の、ホームディレクトリ配下のドットファイルを格納しています。

## vimメモ ##

Neobundle入手は手動で。

    mkdir -p ~/.vim/bundle
    git clone git://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim

vimを起動した後、以下実行。

    cd ~/.vim/bundle/vimproc/
    make

## .vimrc ##

    set list
└ 不可視文字を表示する。

    set listchars=tab:»-,trail:-,nbsp:%,eol:
└ 表示する形式を指定する。

    NeoBundle 'Shougo/unite.vim'
    nmap <Space> [unite]
    nnoremap <silent> [unite]r :<C-u>Unite<Space>register<CR>
└ :Uniteコマンドで統合されたユーザインターフェースを提供する。後述のvimfilerで必要。

    NeoBundle 'Shougo/vimproc'
└ 非同期処理を実現する。

    NeoBundle 'Shougo/vimfiler'
    nnoremap <silent> <Space>d :<C-u>VimFiler<CR>
└ ファイラ。

    NeoBundle 'Shougo/neomru.vim'
└ :Unite file_mruが使えるようにする。

    NeoBundle 'Shougo/neoyank.vim'
└ :Unite history/yankが使えるようにする。

    NeoBundle 'Shougo/neocomplete.vim'
└ 入力補完機能を提供する。

    NeoBundle 'vim-scripts/vim-auto-save'
    let g:auto_save = 1
└ ノーマルモード移行時に自動上書き保存する。

    NeoBundle 'thinca/vim-quickrun'
    let g:quickrun_config={'*': {'split': 'vertical'}}
    set splitright
    nnoremap <silent> qr :<C-u>QuickRun<CR>
└ 編集中のファイルを即時実行する。(paiza用)

    NeoBundle 'szw/vim-tags'
    let g:vim_tags_auto_generate = 1
└ タグジャンプ用のCtagを作成する。

    nnoremap <C-j> :exe("tjump ".expand('<cword>'))<CR>
    nnoremap <C-]> g<C-]>
└ タグジャンプのショートカット設定。

    NeoBundle 'tpope/vim-endwise'
└ 構文終わり(ifに対するendなど)を自動入力する。

## 注意 ##
vimproc_linux64.soが無いと怒られた場合、以下のコマンドを実行する。

    [toshi@tk2-228-23673 ~]$ cd /home/toshi/.vim/bundle/vimproc/
    [toshi@tk2-228-23673 vimproc]$ make
rubocopはインストールしておく。

    # gem install rubocop refe2
    # bitclust setup
