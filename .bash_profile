# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/bin

export PATH

alias glog='git log --date=short --pretty='\''format:%C(yellow)%h %C(green)%cd %C(blue)%an%C(red)%d %C(reset)%s'\'' -10 --graph'
alias gst='git status -s'
alias gb='git branch'
alias gc='git checkout'

