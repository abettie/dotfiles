" -----------------------------------------------------------
" 基本設定
" -----------------------------------------------------------
" 色分けを有効化。
syntax on
" vi互換を無効化し、vim機能を全て使用する。
set nocompatible
" backspase関連設定
"   start: 挿入モード時にbackspeceで文字削除できる。
"   eol: 行頭でbackspace押下時に行連結できる。
"   indent: オートインデントモードのインデントも削除できる。
set backspace=start,eol,indent
" 行番号表示。
set number
" 文字コード設定。
set encoding=utf8
" タブの幅。
set tabstop=2
" <TAB>により挿入されるタブ文字をスペースに変換する。
set expandtab
" 行頭のインデント幅
set shiftwidth=2
" 改行時に前の行のインデントと同じ幅でインデントを挿入する。
set autoindent
" 前の行の行末({など)を見て、次の行でインデントが挿入される。
set smartindent
" .swpファイルを作らない。
set noswapfile
" ~ファイルを作らない。
set nobackup
" 背景色に合わせた色を自動的に設定。
set background=dark
" カラー設定。
"colorscheme hybrid
" タブ、空白、改行を可視化
"set list
" tab: タブの表示。
" trail: 行末に続くスペースの表示。
" eol: 改行の表示。
"set listchars=tab:»-,trail:-,eol:⏎

" -----------------------------------------------------------
" NeoBundle
" -----------------------------------------------------------
if has('vim_starting')
	set rtp+=$HOME/.vim/bundle/neobundle.vim/
endif
call neobundle#begin(expand('~/.vim/bundle'))
NeoBundleFetch 'Shougo/neobundle.vim'
" :Uniteコマンドで統合されたユーザインターフェースを提供する。後述のvimfilerで必要。
NeoBundle 'Shougo/unite.vim'
" 非同期処理を実現する
NeoBundle 'Shougo/vimproc'
" ファイラ
NeoBundle 'Shougo/vimfiler'
" :Unite file_mruが使えるようにする。
NeoBundle 'Shougo/neomru.vim'
" :Unite history/yankが使えるようにする。
NeoBundle 'Shougo/neoyank.vim'
" 入力補完機能を提供する
NeoBundle 'Shougo/neocomplete.vim'
" NeoBundle 'violetyk/neocomplete-php.vim'
" ノーマルモード移行時に自動上書き保存する。
NeoBundle 'vim-scripts/vim-auto-save'
" 編集中のファイルを即時実行する。(paiza用)
NeoBundle 'thinca/vim-quickrun'
" タグジャンプ用のCtagを作成する。
NeoBundle 'szw/vim-tags'
" 構文終わり(ifに対するendなど)を自動入力する。
NeoBundle 'tpope/vim-endwise'
" 構文チェックを行う。
NeoBundle 'scrooloose/syntastic'
" CoffeeScriptのシンタックスカラー
NeoBundle 'kchmck/vim-coffee-script'
call neobundle#end()

" ファイル形式検出及びファイル形式別のプラグイン、インデント有効化。
filetype plugin indent on


" ############### Setting ##################
" ### unite ###
let g:unite_source_history_yank_enable=1
if executable('ag')
	let g:unite_source_grep_command = 'ag'
	let g:unite_source_grep_default_opts = '--nocolor --nogroup'
	let g:unite_source_grep_max_candidates = 200
	let g:unite_source_grep_recursive_opt = ''
endif
nmap <Space> [unite]
nnoremap <silent> [unite]r :<C-u>Unite<Space>register<CR>

" ### VimFiler ###
nnoremap <silent> <Space>d :<C-u>VimFiler<CR>
nnoremap <silent> <Space>b :<C-u>VimFiler /var/www/app/assets/javascripts/backbone<CR>
nnoremap <silent> <Space>a :<C-u>VimFiler /var/www/app/assets<CR>
nnoremap <silent> <Space>m :<C-u>VimFiler /var/www/app/models<CR>
nnoremap <silent> <Space>v :<C-u>VimFiler /var/www/app/views<CR>
nnoremap <silent> <Space>c :<C-u>VimFiler /var/www/app/controllers<CR>

" ### neocomplete ###
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'
" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : ''
    \ }
" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'
" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()
" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? "\<C-y>" : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? "\<C-y>" : "\<Space>"
" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1
" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplete#enable_auto_select = 1
"let g:neocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"
" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
	let g:neocomplete#sources#omni#input_patterns = {}
endif
"let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
"let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
"let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'
" For perlomni.vim setting.
" https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
let g:neocomplete_php_locale = 'ja'

" ### auto save ###
let g:auto_save = 1

" ### syntastic ###
" recommended settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
" display '>>' at syntax error
let g:syntastic_enable_signs = 1
" prevent conflict
let g:syntastic_always_populate_loc_list = 1
" display error list
let g:syntastic_auto_loc_list = 1
" check when opening file
"let g:syntastic_check_on_open = 0
" check when :wq
"let g:syntastic_check_on_wq = 0
" check only ruby file
"let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': ['ruby'], 'passive_filetypes': [] }
"let g:syntastic_ruby_checkers = ['rubocop']
"nnoremap <C-c> :w<CR>:SyntasticCheck<CR>

" ### QuickRun ###
let g:quickrun_config={'*': {'split': 'vertical'}}
set splitright
nnoremap <silent> qr :<C-u>QuickRun<CR>

" tagjump with vsplit window
nnoremap <C-j> :exe("tjump ".expand('<cword>'))<CR>

" list tags if many tags exist
nnoremap <C-]> g<C-]>

" auto tag generate when file saved
let g:vim_tags_auto_generate = 1

" escape insertmode by typing 'jj'
inoremap <silent> jj <ESC>

" ### Other ###
nnoremap <silent> vs :<C-u>vsplit<CR>

NeoBundleCheck
